#ifndef YASND_LOG_H
#define YASND_LOG_H

// Debug verbosity
#define DEBUG_BASE 1
#define DEBUG_ALL 2

// External variables
extern int debug_flag;

// External functions
extern void log_init();
extern void log_close();
extern void log_debug(const char *message,int verbosity);
extern void log_event(const char *message);

#endif

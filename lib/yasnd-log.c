#include <syslog.h>

int debug_flag=0; // debug verbosity flag

void log_init()
{
	return openlog("yasnd", LOG_PID|LOG_CONS, LOG_USER);
}

void log_close()
{
	return closelog();
}

/*
	Function, that called to output various debug messages to syslog
*/
void log_debug(const char *message,int verbosity)
{
	if (debug_flag>=verbosity)
		syslog(LOG_INFO,"%s",message);
}

/*
	Function, that called to output information message to syslog
*/
void log_event(const char *message)
{
	syslog(LOG_INFO,"%s",message);
}

#ifndef YASND_GAMMU_H
#define YASND_GAMMU_H

// External variables
extern char* phone_device; // phone device for gammu, for example: "/dev/ttyACM0"
extern char* phone_model; // gammu phone model, for example: "at"
extern char* phone_connection; // gammu phone connection type, for example: "at"

// External functions
extern bool gammu_init();
extern bool gammu_send_sms(const char* message);
extern bool gammu_read_sms();
extern void gammu_free();

#endif

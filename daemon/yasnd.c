#include "config.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <getopt.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <confuse.h>

#include "yasnd.h"
#include "yasnd-gammu.h"
#include "yasnd-log.h"
#include "yasnd-lpt.h"
#include "yasnd-sock.h"

cfg_t *cfg; // pointer to configuration structure
char pidfile[]="/var/run/yasnd.pid"; // pidfile path
host_decl* hosts=NULL; // structure with hosts' definitions
int hosts_count=0; // count of hosts
bool sms_enable=false; // use SMS messages for control and reports?
char* recipient_number=NULL; // recipient of sms alerts
bool loop_locked=false; // flag for locking main loop while config re-reading
long failures_first=0; // count of failures while hosts checking, that triggers SMS sending
long failures_second=0; // count of failures while hosts checking, that triggers host's reset
long failures_third=0; // count of failures while hosts checking, that clean failure statistics
pthread_t mainloop_handle; // handle of main loop thread

void* allocate_memory(int bytes)
{
	void* result=malloc(bytes);
	if (result==NULL)
	{
		log_event("Error: malloc failed");
		exit(EXIT_FAILURE);
	}
	return result;
}


// Previous function, but without ignoring lpt_enable option
void lpt_lock()
{
	return lpt_lock_cond(false);
}

// Function, that checks that defined host entry is valid
int cb_validate_host(cfg_t *cfg, cfg_opt_t *opt)
{
	return 0; // success
}

void pidfile_init()
{
	int pidfile_d=open(pidfile,O_RDWR|O_CREAT,0640);
	/* Can not open/create pidfile? */
	if (pidfile_d<0)
	{
		log_event("Error: can not open/create pid file");
		exit(EXIT_FAILURE);
	}
	/* Can not lock pidfile? */
	if (lockf(pidfile_d,F_TLOCK,0)<0)
	{
		log_event("Error: can not lock pid file, maybe another yasnd instance still running?");
		exit(EXIT_FAILURE);
	}
	/* First daemon instance continues */
	char pidstr[20];
	sprintf(pidstr,"%d\n",getpid());
	/* Record pid to pidfile and hold it */
	write(pidfile_d,pidstr,strlen(pidstr));
}

void init()
{
	static cfg_opt_t host_opts[] = {
		CFG_STR("hostname", 0, CFGF_NONE),
		CFG_INT("lpt_pin", 0, CFGF_NONE),
		CFG_END()
	};
	// Initialize config parsing library
	cfg_opt_t opts[] = {
		CFG_SEC("host", host_opts, CFGF_MULTI | CFGF_TITLE),
		CFG_SIMPLE_INT("debug", &debug_flag),
		CFG_SIMPLE_INT("failures_first", &failures_first),
		CFG_SIMPLE_INT("failures_second", &failures_second),
		CFG_SIMPLE_INT("failures_third", &failures_third),
		CFG_SIMPLE_BOOL("lpt_enable", &lpt_enable),
		CFG_SIMPLE_INT("lpt_port", &lpt_port),
		CFG_SIMPLE_BOOL("sms_enable", &sms_enable),
		CFG_SIMPLE_STR("phone_device", &phone_device),
		CFG_SIMPLE_STR("phone_model", &phone_connection),
		CFG_SIMPLE_STR("phone_connection", &phone_model),
		CFG_SIMPLE_STR("sms_recipient", &recipient_number),
		CFG_END()
	};
	cfg = cfg_init(opts, CFGF_NONE);
	// set a validating callback function for host sections
	cfg_set_validate_func(cfg, "host", &cb_validate_host);
	// check if config file is valid and parse it
	if (cfg_parse(cfg, "/etc/yasnd.conf") == CFG_PARSE_ERROR)
	{
		log_event("Error parsing config file");
		exit(EXIT_FAILURE);
	}
	// failures thresholds must be explicitly defined in config
	if (failures_first==0 || failures_second==0 || failures_third==0)
	{
		log_event("Error: all failures threshold parameters must be defined in config file");
		exit(EXIT_FAILURE);
	}
	// if SMS is enabled, all of phone parameters must be explicitly defined
	if (sms_enable)
	{
		if (phone_model==NULL)
		{
			log_event("Error: phone model is not defined in config file");
			exit(EXIT_FAILURE);
		}
		if (phone_connection==NULL)
		{
			log_event("Error: phone connection type is not defined in config file");
			exit(EXIT_FAILURE);
		}
		if (phone_device==NULL)
		{
			log_event("Error: phone device is not defined in config file");
			exit(EXIT_FAILURE);
		}
		if (recipient_number==NULL)
		{
			log_event("Error: no recipient for sms alerts defined in config file");
			exit(EXIT_FAILURE);
		}
	}
	// hosts' array must not be empty
	hosts_count=cfg_size(cfg,"host");
	if (hosts_count==0)
	{
		log_event("Error: no hosts to check defined in config file");
		exit(EXIT_FAILURE);
	}
	// allocate memory for hosts array...
	hosts=allocate_memory(hosts_count*sizeof(host_decl));
	// ...and fill it with config file content
	for (int i=0;i<hosts_count;i++)
	{
		cfg_t* host = cfg_getnsec(cfg, "host", i);
		hosts[i].hostname=cfg_getstr(host,"hostname");
		hosts[i].lpt_pin=cfg_getint(host,"lpt_pin");
		hosts[i].fail_count=0;
		hosts[i].alert_sent=false;
		hosts[i].reaction_obtained=false;
	}
#ifdef HAVE_LIBGAMMU
	// initialize gammu structures
	if (sms_enable)
		gammu_init();
#endif
	// initialize LPT control circuit
	lpt_init();
	// initialize server socket
	sock_init();
}

void check_host(int num,host_decl* host)
{
	pid_t pid = fork();
	if (pid < 0) {
		log_event("Error: can not fork child :(");
                exit(EXIT_FAILURE);
        }
        // If pid == 0, then we are in the child
        if (pid == 0) {
		// Set default signal handlers
		signal(SIGTERM, SIG_DFL);
		signal(SIGHUP, SIG_DFL);
		// Define temporary variable
		char tmp[100];
		// Get host address info from hostname
		struct addrinfo hints, *res;
		memset(&hints, 0, sizeof(hints));
		//
		// STUB: need to check IPv6 support for current host here!
		//
		// prefer any address family(both IPv4 and IPv6)
		hints.ai_family = AF_UNSPEC;
		if ((getaddrinfo(host->hostname, NULL, &hints, &res)) != 0)
		{
			sprintf(tmp, "Error: Failed to resolve hostname '%s'", host->hostname);
			log_event(tmp);
			exit(EXIT_FAILURE);
		}
		sprintf(tmp, "Pinging host %s", host->hostname);
		log_debug(tmp,DEBUG_BASE);
		// host is IPv6 capable?
		if (res->ai_family == AF_INET6)
		{
			execl("/bin/ping6","/bin/ping6","-c 1","-n",host->hostname,(char*) 0);
		}
		else
		// host is IPv4 capable?
		{
			execl("/bin/ping","/bin/ping","-c 1","-n",host->hostname,(char*) 0);
		}
		// STUB: check result of exec call
		exit(EXIT_SUCCESS);
        }
	// Save child's pid
	host->helper_pid=pid;
}

void* loop_function(void* param)
{
	for (int i=0;i<hosts_count;i++)
	{
		check_host(i,&hosts[i]);
	}
	for (int i=0;i<hosts_count;i++)
	{
		int pid_status;
		int result = waitpid(hosts[i].helper_pid, &pid_status, 0);
		if (result > 0)
		{
			if (WIFEXITED(pid_status))
			{
				char tmp[50];
				sprintf(tmp, "Finished child %d with result %d", result, WEXITSTATUS(pid_status));
				log_debug(tmp,DEBUG_ALL);
				// if host is alive - reset failure counter
				// otherwise - increment it
				if (WEXITSTATUS(pid_status) == 0)
				{
					hosts[i].fail_count=0;
					hosts[i].alert_sent=false;
					hosts[i].reaction_obtained=false;
				}
				else
					hosts[i].fail_count++;
			}
		}
	}
	for (int i=0;i<hosts_count;i++)
	{
		mymsgbuf qbuf;
		if (hosts[i].fail_count>failures_second && hosts[i].alert_sent && !hosts[i].reaction_obtained)
		{
			if (lpt_enable)
			{
				qbuf.mtype=SMS_SEND_TYPE;
				sprintf(qbuf.mtext,"Host %s does not answer and no reaction on this. Trying to reset it(LPT pin %d)",hosts[i].hostname,hosts[i].lpt_pin);
				reset_pin(hosts[i].lpt_pin);
				hosts[i].reaction_obtained=true;
				// send message with SMS text to IPC queue
				send_message(qid,&qbuf);
			}
			continue;
		}
		if (hosts[i].fail_count>failures_first && !hosts[i].alert_sent)
		{
			qbuf.mtype=SMS_SEND_TYPE;
			sprintf(qbuf.mtext,"Host %s does not answer(LPT pin %d)",hosts[i].hostname,hosts[i].lpt_pin);
			// set alert flag to prevent sending more than 1 message
			// for unreachable host
			hosts[i].alert_sent=true;
			// send message with SMS text to IPC queue
			send_message(qid,&qbuf);
			continue;
		}
	}
	return NULL;
}

void signal_handler(int signum)
{
	void free_resources()
	{
		// lock Main Loop
		loop_locked=true;
		// kill Main Loop thread
		pthread_cancel(mainloop_handle);
		// Stop IPC loop and remove IPC queue
		ipc_free();
		// free config structure
		cfg_free(cfg);
#ifdef HAVE_LIBGAMMU
		// free gammu structure
		gammu_free();
#endif
		// free hosts structures memory
		if (hosts!=NULL)
			free(hosts);
		// remove pid file
		remove(pidfile);
	}

	if (signum==SIGHUP)
	{
		log_event("SIGHUP captured, daemon re-read config");
		// free all resources
		free_resources();
		// Init IPC loop again
	        ipc_init();
		// run init again
		init();
		// sleep 5 seconds and unlock Main Loop
		sleep(5);
		loop_locked=false;
		return;
	}
	if (signum==SIGTERM)
	{
		log_event("SIGTERM captured, daemon stopping");
		// free resources
		free_resources();
		// block LPT control circuit
		lpt_lock();
		// close log decriptor
		log_close();
		// close server socket
		sock_close();
		//
		exit(EXIT_SUCCESS);
	}
}

void arg_parse(int argc, char *argv[])
{
	int c;
	struct option option_string[] =
	{
		{"init", no_argument, NULL, 'i'},
	};
	while ((c = getopt_long(argc, argv,"i", option_string, NULL)) != -1)
	{
		switch(c)
		{
			case 'i':
				lpt_lock_cond(true);
				exit(EXIT_SUCCESS);
				break;
			default:
				break;
		}
	}
}

int main(int argc, char *argv[])
{
	// Parse command line arguments
	arg_parse(argc,argv);

	/* Our process ID and Session ID */
	pid_t pid, sid;

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	}
	/* If we got a good PID, then we can exit the parent process. */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* Open any logs here */
	log_init();

	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0) {
		/* Log the failure */
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		/* Log the failure */
		exit(EXIT_FAILURE);
	}

	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	// create pid file and write current process pid
	pidfile_init();

	// Init apropriate structures
	init();

	// initialize IPC queue and create process
	// that will watch for new messages in it
	ipc_init();

	// Set signal handlers
	signal(SIGTERM, signal_handler);
	signal(SIGHUP, signal_handler);

	/* The Main Loop */
	while (1) {
		if (!loop_locked)
		{
			if(pthread_create(&mainloop_handle,NULL,loop_function, NULL) != 0)
			{
				log_event("Thread creation failed");
			}
			sleep(60); /* wait 60 seconds */
			// wait for the completion of thread
			pthread_join(mainloop_handle,NULL);
//			log_event("Exiting");
//			exit(EXIT_SUCCESS);
		}
		else
		{
			// if loop is locked, just wait 1 second and start it again
			sleep(1);
		}
	}
	exit(EXIT_SUCCESS);
}

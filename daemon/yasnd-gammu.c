#include "config.h"
#include "yasnd.h"
#include "yasnd-log.h"
#ifdef HAVE_LIBGAMMU
#include <gammu.h>
#endif

char* phone_device=NULL; // phone device for gammu, for example: "/dev/ttyACM0"
char* phone_model=NULL; // gammu phone model, for example: "at"
char* phone_connection=NULL; // gammu phone connection type, for example: "at"

#ifdef HAVE_LIBGAMMU
GSM_Error error; // structure to store possible gammu errors
GSM_StateMachine *state_machine=NULL; // structure to interact with mobile phones
volatile gboolean gammu_shutdown=FALSE; // variable that indicates gammu shutdown
volatile GSM_Error sms_send_status;

bool check_gammu_error(GSM_Error err)
{
	if (err == ERR_NONE) {
		// No error, return OK('true')
		return true;
	}
	char tmp[150];
	sprintf(tmp,"gammu failure: %s\n", GSM_ErrorString(err));
	log_event(tmp);
	return false;
}

bool gammu_read_sms()
{
	GSM_MultiSMSMessage sms;

	// Variable for SMS sender number
	char sender_number[20];
	memset(&sender_number, 0, sizeof(sender_number));

	/* Connect to phone */
	/* 1 means number of replies you want to wait for */
	error = GSM_InitConnection(state_machine, 1);
	if (!check_gammu_error(error))
                return false;

	/* Prepare for reading message */
	error = ERR_NONE;
	sms.Number = 0;
	sms.SMS[0].Location = 0;
	sms.SMS[0].Folder = 0;

	log_debug("Trying to read SMS...",DEBUG_ALL);

	error = GSM_GetNextSMS(state_machine, &sms, TRUE);
	if (error == ERR_EMPTY) return true;
	if (!check_gammu_error(error))
		return false;

	// Now we can do something with the message
	for (int i = 0; i < sms.Number; i++)
	{
		strncpy(sender_number,DecodeUnicodeConsole(sms.SMS[i].Number),19);
		char dbg_tmp[1000];
		sprintf(dbg_tmp,"Number: '%s'", sender_number);
		log_debug(dbg_tmp,DEBUG_ALL);

		// special hack for Motorola L7
		sms.SMS[i].Location-=100000;

		if (sms.SMS[i].Coding == SMS_Coding_8bit) {
			log_event("gammu sms error: 8-bit message, can not read");
			// remove SMS
			GSM_DeleteSMS(state_machine,&sms.SMS[i]);
			return false;
		} else {
			sprintf(dbg_tmp,"Text: \"%s\"\n", DecodeUnicodeConsole(sms.SMS[i].Text));
			log_debug(dbg_tmp,DEBUG_ALL);
		}

		// remove SMS after reading
		error = GSM_DeleteSMS(state_machine,&sms.SMS[i]);
		if (!check_gammu_error(error))
	                return false;
	}

	/* Terminate connection */
	error = GSM_TerminateConnection(state_machine);
	if (!check_gammu_error(error))
                return false;
	return true;
}

bool gammu_init()
{
	GSM_InitLocales(NULL);
	state_machine = GSM_AllocStateMachine();
	/* Set gammu configuration */
	GSM_Config *gammu_cfg = GSM_GetConfig(state_machine, 0);
	// first, free old values and set new one
	free(gammu_cfg->Device);
	gammu_cfg->Device = strdup(phone_device);
	free(gammu_cfg->Connection);
	gammu_cfg->Connection = strdup(phone_connection);
	strcpy(gammu_cfg->Model, phone_model);
	/* We care only about first configuration */
	GSM_SetConfigNum(state_machine, 1);
	return true;
}

void gammu_free()
{
	GSM_FreeStateMachine(state_machine);
}

/* Handler for SMS send reply */
void send_sms_callback (GSM_StateMachine *sm, int status, int MessageReference, void * user_data)
{
	char tmp[100];
	if (status==0) {
		sprintf(tmp,"Sent SMS on device '%s' - OK\n", GSM_GetConfig(sm, -1)->Device);
		sms_send_status = ERR_NONE;
		log_debug(tmp,DEBUG_BASE);
	} else {
		sprintf(tmp,"Sent SMS on device '%s' - FAILURE\n", GSM_GetConfig(sm, -1)->Device);
		sms_send_status = ERR_UNKNOWN;
		log_event(tmp);
	}
}

bool gammu_send_sms(const char* message)
{
	GSM_SMSMessage sms;
	GSM_SMSC PhoneSMSC;
	int return_value = 0;

	/* Prepare message */
	// Cleanup the structure
	memset(&sms, 0, sizeof(sms));
	// Encode message text
	EncodeUnicode(sms.Text, message, strlen(message));
	// Encode recipient numbe
	EncodeUnicode(sms.Number, recipient_number, strlen(recipient_number));
	/* We want to submit message */
	sms.PDU = SMS_Submit;
	/* No UDH, just a plain message */
	sms.UDH.Type = UDH_NoUDH;
	/* We used default coding for text */
	sms.Coding = SMS_Coding_Default_No_Compression;
	/* Class 1 message (normal) */
	sms.Class = 1;

	/* Connect to phone */
	/* 1 means number of replies you want to wait for */
	error = GSM_InitConnection(state_machine, 1);
	if (!check_gammu_error(error))
		return false;

	/* Set callback for message sending */
	/* This needs to be done after initiating connection */
	GSM_SetSendSMSStatusCallback(state_machine, send_sms_callback, NULL);

	/* We need to know SMSC number */
	PhoneSMSC.Location = 1;
	error = GSM_GetSMSC(state_machine, &PhoneSMSC);
	if (!check_gammu_error(error))
		return false;

	/* Set SMSC number in message */
	CopyUnicodeString(sms.SMSC.Number, PhoneSMSC.Number);

	log_debug("Prepare to send SMS...",DEBUG_ALL);
	/* Send message */
	error = GSM_SendSMS(state_machine, &sms);
	if (!check_gammu_error(error))
		return false;

	while (!gammu_shutdown) {
		GSM_ReadDevice(state_machine, TRUE);
		if (sms_send_status == ERR_NONE) {
			/* Message sent OK */
			return true;
		}
		if (sms_send_status != ERR_TIMEOUT) {
			/* Message sending failed */
			return false;
		}
	}
	return true;
}
#endif

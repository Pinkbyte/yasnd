#ifndef YASND_H
#define YASND_H

// IPC message types
#define SMS_SEND_TYPE 1
#define SMS_RECEIVE_TYPE 2

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#define _GNU_SOURCE
#include <sched.h>

// Structure, that described single checked host
typedef struct {
	char* hostname; // address of host
	int lpt_pin; // pin on LPT control circuit for reset
	pid_t helper_pid; // pid of helper('pinger') child process
	int fail_count; // how many times in a row host was unreachable
	bool alert_sent; // variable, that changed when host goes online/offline
	bool reaction_obtained; // variable, that changed when reaction on it's behaviour obtained
} host_decl;

// Structure, that described single IPC message
typedef struct {
	long mtype;         /* Message type */
	char mtext[100];
} mymsgbuf;

// External functions
extern void* allocate_memory(int bytes);
extern void ipc_init();
extern void ipc_free();
extern int send_message(int qid, mymsgbuf* qbuf);

// External variables
extern int hosts_count; // count of hosts
extern char* recipient_number;
extern bool sms_enable;
extern int qid;

#endif

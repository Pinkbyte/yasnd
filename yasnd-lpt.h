#ifndef YASND_LPT_H
#define YASND_LPT_H

#include <stdbool.h>

// External variables
extern bool lpt_enable; // control usage of LPT port to reset target devices
extern int lpt_port; // LPT port in hex(0x378 is usually LPT1)

// External functions
extern void lpt_init();
extern void lpt_lock_cond(bool ignore_options);
extern void lpt_lock();
extern void reset_pin(int pin_num);

#endif
